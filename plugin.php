<?php
namespace Webforia_User_Price;

class Plugin_Init
{

    public function __construct()
    {
        load_plugin_textdomain(WEBFORIA_USER_PRICE_DOMAIN, false, dirname(plugin_basename(__FILE__)) . '/languages/');
        add_filter('body_class', [$this, 'add_body_class']);
    }


    /**
     * Added body css class
     *
     * @param [type] $classes css class
     * @return void
     */
    public function add_body_class($classes)
    {
        $classes[] = 'webforia-user-price';

        return $classes;
    }

}