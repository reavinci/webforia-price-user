<?php
/**
 * Register form admin 
 * 
 * Handle price user admin
 * @version 1.0.0
 */
namespace Webforia_User_Price;

class Reseller
{

    public function __construct()
    {

        // add custom field on simple product
        add_action('woocommerce_product_options_general_product_data', [$this, 'add_simple_metabox']);
        add_action('woocommerce_process_product_meta', [$this, 'save_simple_metabox']);

        // add custom field on variation product
        add_action('woocommerce_variation_options_pricing', [$this, 'add_variation_metabox'], 10, 3);
        add_action('woocommerce_save_product_variation', [$this, 'save_variation_metabox'], 10, 2);
        add_filter('woocommerce_available_variation', [$this, 'save_available_variation']);

        // new role on wordpress
        add_action('woocommerce_init', [$this, 'add_new_role']);
        
    }

    /**
     * Added metabox for simple product
     *
     * @hook action woocommerce_product_options_general_product_data
     * @return void
     */
    public function add_simple_metabox()
    {
        global $woocommerce, $post;
        $currency = ' (' . get_woocommerce_currency_symbol() . ')';

        woocommerce_wp_text_input(
            array(
                'id' => 'wsb_reseller_price',
                'class' => 'short',
                'label' => __("Reseller Price {$currency}", WEBFORIA_USER_PRICE_DOMAIN),
                'value' => get_post_meta($post->ID, 'wsb_reseller_price', true),
                'data_type' => 'price',
                'custom_attributes' => array(
                    'step' => 'any',
                    'min' => '1',
                ),
            )
        );
        woocommerce_wp_text_input(
            array(
                'id' => 'wsb_agen_price',
                'label' => __("Agen Price {$currency}", WEBFORIA_USER_PRICE_DOMAIN),
                'value' => get_post_meta($post->ID, 'wsb_agen_price', true),
                'data_type' => 'price',
                'custom_attributes' => array(
                    'step' => 'any',
                    'min' => '1',
                ),
            )
        );
    }

    /**
     * Save metabox for simple product
     *
     * @hook action woocommerce_process_product_meta
     * @param [type] $post_id
     * @return void
     */
    public function save_simple_metabox($post_id)
    {
        $reseller = (intval($_POST["wsb_reseller_price"]) !== 0)? intval($_POST["wsb_reseller_price"]): '';
        $agen = (intval($_POST["wsb_agen_price"]) !== 0) ? intval($_POST["wsb_agen_price"]) : '';

        update_post_meta($post_id, 'wsb_reseller_price', esc_attr($reseller));
        update_post_meta($post_id, 'wsb_agen_price', esc_attr($agen));

    }

    /**
     * Added metabox each variation product data
     *
     * @param [type] $loop variantion loop field
     * @param [type] $variation_data
     * @param [type] $variation get variantion data
     * @hook action woocommerce_variation_options_pricing
     * @return void
     */
    public function add_variation_metabox($loop, $variation_data, $variation)
    {
        $currency = ' (' . get_woocommerce_currency_symbol() . ')';

        woocommerce_wp_text_input(
            array(
                'id' => 'wsb_reseller_price_' . $loop,
                'class' => 'short',
                'wrapper_class' => 'form-row form-row-first',
                'label' => __("Reseller Price {$currency}", WEBFORIA_USER_PRICE_DOMAIN),
                'value' => get_post_meta($variation->ID, 'wsb_reseller_price', true),
                'data_type' => 'price',
                'custom_attributes' => array(
                    'step' => 'any',
                    'min' => '1',
                ),
            )
        );
        woocommerce_wp_text_input(
            array(
                'id' => 'wsb_agen_price_' . $loop,
                'class' => 'short',
                'wrapper_class' => 'form-row form-row-last',
                'label' => __("Agen Price {$currency}", WEBFORIA_USER_PRICE_DOMAIN),
                'value' => get_post_meta($variation->ID, 'wsb_agen_price', true),
                'data_type' => 'price',
                'custom_attributes' => array(
                    'step' => 'any',
                    'min' => '1',
                ),
            )
        );

    }

    /**
     * Save metabox each variation product
     *
     * @param [type] $variation_id
     * @param [type] $index
     * @hook action woocommerce_save_product_variation
     * @return void
     */
    public function save_variation_metabox($variation_id, $index)
    {

        $reseller = (intval($_POST["wsb_reseller_price_{$index}"]) !== 0)? intval($_POST["wsb_reseller_price_{$index}"]): '';
        $agen = (intval($_POST["wsb_agen_price_{$index}"]) !== 0) ? intval($_POST["wsb_agen_price_{$index}"]) : '';

        update_post_meta($variation_id, 'wsb_reseller_price', esc_attr($reseller));
        update_post_meta($variation_id, 'wsb_agen_price', esc_attr($agen));
    }

    /**
     * Save price by user data to variation data attribute on frontend
     *
     * Send data to data-product_variations html data attribute on product variation
     * the attrubute can you access wih js variation event
     * @param [type] $variations
     * @hook filter woocommerce_available_variation
     * @return json
     */
    public function save_available_variation($variations)
    {

        $product = wc_get_product($variations['variation_id']);

        // price by role
        $variations['display_reseller_price'] = intval(get_post_meta($product->get_id(), 'wsb_reseller_price', true));
        $variations['display_agen_price'] = intval(get_post_meta($product->get_id(), 'wsb_agen_price', true));

        // date sale
        $dateFrom = date('Y/m/d', strtotime($product->get_date_on_sale_from()));
        $dateTo = date('Y/m/d', strtotime($product->get_date_on_sale_to()));
        $dateNow = date("Y/m/d");

        if (!empty($dateTo) && $product->is_on_sale()) {
            $variations['sale_price_dates_from'] = $dateFrom;
            $variations['sale_price_dates_to'] = $dateTo;
        }

        // sale percentage
        $percentage = (((int) $product->get_regular_price() - (int) $product->get_sale_price()) / (int) $product->get_regular_price()) * 100;
        $variations['sale_price_percentage'] = round($percentage);

        return $variations;
    }

    /**
     * Create new user role
     *
     * @return void
     */
    public function get_new_roles()
    {
        $roles = array(
            'reseller' => array(
                'role' => 'reseller',
                'name' => 'Reseller',
                'caps' => array(
                    'read' => true,
                ),
            ),
            'agen' => array(
                'role' => 'agen',
                'name' =>'Agen',
                'caps' => array(
                    'read' => true,
                ),
            ),
        );
        return $roles;
    }

    /**
     * Added new role on wordpress user
     *
     * @hook action woocommerce_init
     * @return void
     */
    public function add_new_role()
    {

        foreach ($this->get_new_roles() as $key => $role) {
            add_role($role['role'], $role['name'], $role['caps']);
        }

    }

}
