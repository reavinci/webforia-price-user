<?php
/**
 * Added custom price on front end
 *
 * @version 1.0.0
 * @see  @see https://awhitepixel.com/blog/change-prices-woocommerce-by-code/
 */
namespace Webforia_User_Price;

class Reseller_Price extends Reseller
{
    public function __construct()
    {

        // add_filter('woocommerce_product_get_regular_price', [$this, 'custom_price'], 99, 2);
        add_filter('woocommerce_product_get_price', [$this, 'custom_price'], 99, 2);
        add_filter('woocommerce_product_get_sale_price', [$this, 'custom_price'], 99, 2);

        // Variable (if each product variation have different price)
        // add_filter('woocommerce_product_variation_get_regular_price', [$this, 'custom_price'], 99, 2);
        add_filter('woocommerce_product_variation_get_price', [$this, 'custom_price'], 99, 2);
        add_filter('woocommerce_product_variation_get_sale_price', [$this, 'custom_price'], 99, 2);

        // Variations (of a variable product)
        // add_filter('woocommerce_variation_prices_regular_price', [$this, 'custom_regular_price'], 99, 3);
        add_filter('woocommerce_variation_prices_price', [$this, 'custom_variation_price'], 99, 3);
        add_filter('woocommerce_variation_prices_sale_price', [$this, 'custom_variation_price'], 99, 3);

        add_action('woocommerce_after_shop_loop_item_title', [$this, 'shop_price_display'], 10, 2);
        add_action('woocommerce_single_product_summary', [$this, 'product_price_display'], 10, 2);

    }

    /**

    /**
     * Custom price for simple product
     * Custom variation price if each variation have different price
     */
    public function custom_price($price, $product)
    {
        wc_delete_product_transients($product->get_id());

        return $this->get_user_price($price, $product);

    }

    /*
     * Custom variation price if each variation have same price
     */
    public function custom_variation_price($price, $variation, $product)
    {
        wc_delete_product_transients($variation->get_id());

        return $this->get_user_price($price, $variation);

    }

    /* Show user price on shop page
     *
     * @hook woocommerce_after_shop_loop_item_title
     * @param [type] $price
     * @param [type] $product
     * @return html
     */
    public function shop_price_display()
    {
        if (rt_option('user_price_on_all_user', true) && rt_option('user_price_on_shop', false)) {
            wup_get_template_part('shop-user-price');
        }
    }

    /* Show user price on product page
     *
     * @hook woocommerce_after_shop_loop_item_title
     * @param [type] $price
     * @param [type] $product
     * @return html
     */
    public function product_price_display()
    {
        if (rt_option('user_price_on_all_user', true)) {
            wup_get_template_part('product-user-price');
        }
    }

    /**
     * Check product has custom price
     *
     * @return void
     */
    public function check_user_price()
    {
        $reseller_price = get_post_meta(get_the_ID(), "wsb_reseller_price", true);
        $agen_price = get_post_meta(get_the_ID(), "wsb_agen_price", true);

        if ($reseller_price || $agen_price) {
            return true;
        }
    }

    /**
     * Get user price by role user
     * Get price from metabox
     *
     * @param $price
     * @param $product
     * @return void
     */
    public function get_user_price($price, $product)
    {
        $product_id = $product->get_id();

        $user_price = get_post_meta($product_id, 'wsb_' . wup_current_user_role() . '_price', true);

        if (!empty($user_price)) {
            $price = $user_price;
        }

        return $price;
    }

}
