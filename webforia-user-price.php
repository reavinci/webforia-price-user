<?php
/**
 * Plugin Name: Webforia User Price
 * Description: Plugin WooCommerce untuk memberikan harga khusus untuk member dengan role agen dan reseller
 * Plugin URI:  https://webforia.id
 * Version:     1.0.1
 * Author:     Webforia Studio
 * Author URI:  https://webforia.id
 * Text Domain: webforia-user-price
 * Tags: shop
 * Requires at least: 5.0
 * Tested up to: 5.6
 * Stable tag: 2.0
 * Requires PHP: 7.0
 * WC requires at least: 5.0
 * WC tested up to: 5.0
 */

if (!defined('ABSPATH')) {
    exit;
}

define('WEBFORIA_USER_PRICE_TEMPLATE', plugin_dir_path(__FILE__));
define('WEBFORIA_USER_PRICE_ASSETS', plugin_dir_url(__FILE__));
define('WEBFORIA_USER_PRICE_DOMAIN','webforia-user-price');

/*=================================================;
/* LOAD THIS PLUGIN AFTER RETHEME LOAD
/*================================================= */
function wup_plugin_loaded()
{
    // load woocommerce if woocommerce plugin active
    if (class_exists('WooCommerce')) {
        require_once WEBFORIA_USER_PRICE_TEMPLATE . '/plugin.php';
        require_once WEBFORIA_USER_PRICE_TEMPLATE . '/functions.php';
        require_once WEBFORIA_USER_PRICE_TEMPLATE . '/includes/include.php';
        
        // run class
        New Webforia_User_Price\Plugin_Init;
        New Webforia_User_Price\Customizer;
        New Webforia_User_Price\Reseller;
        New Webforia_User_Price\Reseller_Price;

    }

    // update plugin
    $check_update = Puc_v4_Factory::buildUpdateChecker('https://gitlab.com/rereyossi/webforia-user-price', __FILE__, 'webforia-user-price');
    $check_update->setBranch('stable_release');

    // call back after this plugin loaded
    do_action( 'wup_after_plugin_loaded');
}

add_action('rt_after_setup_theme', 'wup_plugin_loaded');